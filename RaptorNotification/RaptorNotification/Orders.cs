﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaptorNotification
{
    class Orders
    {
        public string orderId;
        public string salesNo;
        public string sale_no;
        public string buzzerNo;
        public string merchantKey;
        public string merchant_key;
        public string paymentType;
        public string queueNo;
        public string queue_no;
        public string status;
        public int isVoid;
        public string pin;

    }
}
