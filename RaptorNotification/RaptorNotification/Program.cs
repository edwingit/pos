﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;


namespace RaptorNotification
{
    public class Program
    {
        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);
        private static bool isclosing = false;
        public static String KIOSK_POS = ConfigurationSettings.AppSettings["KIOSK_POS"];
        public static String END_POINT_GET_ID = ConfigurationSettings.AppSettings["API_LOCALSERVICE_GET_ID"];


        static void Main(string[] args)
        {
            Notification conn = new Notification();

            CreateFolderIfNotExist("orders");
            
            if (conn.IsServerConnected())
            {
                
                Timer t = new Timer(TimerCallback, null, 0, 500);

            }
            else
            {
                Console.WriteLine("Failed Connect to DB.");
            }


            SetConsoleCtrlHandler(new HandlerRoutine(ConsoleCtrlCheck), true);

            while (!isclosing) ;


            Console.ReadLine();


        }

        public static void CreateFolderIfNotExist(string path)
        {
            var exists = Directory.Exists(path);

            if (exists) return;

            Directory.CreateDirectory(path);
        }

        public enum CtrlTypes

        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)

        {
            switch (ctrlType)
            {

                case CtrlTypes.CTRL_C_EVENT:

                    isclosing = true;

                    Console.WriteLine("CTRL+C received!");

                    break;
        
                case CtrlTypes.CTRL_BREAK_EVENT:

                    isclosing = true;

                    Console.WriteLine("CTRL+BREAK received!");

                    break;
                    
                case CtrlTypes.CTRL_CLOSE_EVENT:

                    isclosing = true;

                    break;
                    
                case CtrlTypes.CTRL_LOGOFF_EVENT:

                case CtrlTypes.CTRL_SHUTDOWN_EVENT:

                    isclosing = true;

                    break;
                    
            }

            return true;

        }


        private static void TimerCallback(Object o)
        {
            Notification ne = new Notification();
            ne.StartNotification();
            ne.StopNotification();
            GC.Collect();
        }
    }

    public class Notification
    {   
        private delegate void RateChangeNotification(DataTable table);
        private SqlDependency dependency;
        //string ConnectionString = "Data Source = DESKTOP-7RQIARR\\SQLEXPRESS;Initial Catalog = ServiceBrokerTest; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //string ConnectionString = "Data Source = POS001;Initial Catalog = Raptor; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public static String CONNECTION = ConfigurationSettings.AppSettings["CONNECTION"];
        public static String API_IP = ConfigurationSettings.AppSettings["API_IP"];
        public static String API_PORT = ConfigurationSettings.AppSettings["API_PORT"];
        public static String API_LOCALSERVICE = ConfigurationSettings.AppSettings["API_LOCALSERVICE"];
        public static String MERCHANT_KEY = ConfigurationSettings.AppSettings["MERCHANT_KEY"];
        public static String KIOSK_POS = ConfigurationSettings.AppSettings["KIOSK_POS"];
        public static String END_POINT_GET_ID = ConfigurationSettings.AppSettings["API_LOCALSERVICE_GET_ID"];

        public void StartNotification()
        {
            try
            {
                
                //SqlDependency.Start(this.ConnectionString, "SBReceiveQueue");
                SqlDependency.Start(CONNECTION);
                SqlConnection connection = new SqlConnection(CONNECTION);
                SqlDataReader myreader;
                connection.Open();

                var endPointInsert = "http://"+ API_IP + ":"+ API_PORT+"/rest/orders/dummy/";
                var endPointUpdate = "http://" + API_IP + ":" + API_PORT + "/rest/orders/update/";
                var endPointVoid = "http://" + API_IP + ":" + API_PORT + "/rest/orders/void/";
                var endPoint = String.Empty;
                
                // api kiosk status 2:ready 3:delivered

                using (SqlCommand command =new SqlCommand("RECEIVE CONVERT(NVARCHAR(MAX), message_body) AS Message FROM SBReceiveQueue", connection))
                {
                    myreader = command.ExecuteReader();

                    while (myreader.Read())
                    {

                        var str = myreader[0].ToString();
                        string[] strList = str.Split('#');

                        var orderId = "";
                        var queueNo = strList[1];
                        var type = strList[5].ToUpper();
                        var status = strList[2];
                        var pos = strList[3];
                        var sale = strList[4];
                        bool toKDS = true;
                        bool onPrepare = false;
                        bool onClose = false;
                        var selectedFile = String.Empty;

                        try
                        {
                            
                            if (type == "INSERT")
                            {
                                queueNo = strList[1];
                                status = "1";
                                endPoint = endPointInsert;

                            }
                            else
                            {
                                string[] files = Directory.GetFiles("orders");
                                foreach (string file in files)
                                {
                                    var filename = Path.GetFileName(file);
                                    var a = filename.Split('-');
                                    var q = queueNo;

                                    if (a[1].Contains(q))
                                    {
                                        
                                        string text = File.ReadAllText(file);
                                        var t = text.Split('#');

                                        Console.WriteLine("last status : "+t[2]);
                                        if (t[2] == "1") {
                                            onPrepare = true;
                                            File.WriteAllText(string.Concat(new string[] { "orders/", a[0] + "-" + queueNo, ".txt" }), a[0] + "#" + queueNo + "#1");

                                        }

                                        if (t[2] == "2") {
                                            onClose = true;
                                            File.WriteAllText(string.Concat(new string[] { "orders/", a[0] + "-" + queueNo, ".txt" }), a[0] + "#" + queueNo + "#2");

                                        }


                                        orderId = a[0];
                                        selectedFile = filename;
                                        Console.WriteLine("Found id : " + orderId);

                                    }

                                }

                                endPoint = endPointUpdate;
                            }


                            //status = status == "0" ? "3" : status; //0 in raptor = 2 in kiosk (Delivered)
                            var isVoid=0;

                            if (status == "0")
                            {
                                status = "3";//close the order in MC
                                isVoid = 0;

                            }
                            else if (status == "3")//3 or void from raptor
                            {
                                isVoid = 1;
                                endPoint = endPointVoid;
                            }
                            

                            string orderFrom = String.Empty;
                            
                            if (KIOSK_POS.Contains(pos))
                            {
                                if (isVoid == 1)
                                {
                                    endPoint = endPointVoid;
                                }
                                else
                                {
                                    endPoint = endPointUpdate;
                                }
                                
                                
                                orderFrom = "Kiosk";
                                queueNo = queueNo.Trim(new Char[] { ' ', 'K', '.' });
                                queueNo.Replace(System.Environment.NewLine, " ");

                                if (type == "UPDATE")
                                {
                                    Console.WriteLine(">>>>>>>>>>>>>>>>> QUEUE : " + queueNo);
                                    
                                    var req = (HttpWebRequest)WebRequest.Create(END_POINT_GET_ID);

                                    try
                                    {

                                        Orders Ids = new Orders();

                                        Ids.orderId = "";
                                        Ids.buzzerNo = "";
                                        Ids.sale_no = "";
                                        Ids.merchant_key = "5d4a9fe9b959e";
                                        Ids.paymentType = "CASHPAID";
                                        Ids.queue_no = queueNo;
                                        Ids.status = "";

                                        var datas = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(Ids));

                                        req.Method = "POST";
                                        req.ContentType = "application/json";
                                        req.ContentLength = datas.Length;

                                        using (var stream = req.GetRequestStream())
                                        {
                                            stream.Write(datas, 0, datas.Length);
                                        }

                                        var responseId = (HttpWebResponse)req.GetResponse();

                                        var responseStr = new StreamReader(responseId.GetResponseStream()).ReadToEnd();

                                        var resp1 = (JObject)JsonConvert.DeserializeObject(responseStr);

                                        var id = resp1.SelectToken("data.order_id").ToString();

                                        Console.WriteLine(">>>>>>>>>>>>>>>>> GET ID : " + id);
                                        orderId = id;



                                    }
                                    catch (Exception t)
                                    {
                                        Console.WriteLine(t.Message);
                                    }
                                }
                                else
                                {
                                    toKDS = false;
                                    Console.WriteLine("No Action");
                                }

                            }
                            else
                            {
                                
                                toKDS = true;
                                orderFrom = "Raptor";
                            }


                            if (toKDS)
                            {
                                /////////******UPDATE TO KIOSK******///////////////
                                Console.WriteLine("Received     : " + myreader[0].ToString().Trim().Replace(System.Environment.NewLine, " "));
                                Console.WriteLine("Trigger      : " + type);
                                Console.WriteLine("Order From   : " + orderFrom);
                                Console.WriteLine("Api Endpoint : " + endPoint);
                                Console.WriteLine("Queue / Table: " + queueNo);
                                Console.WriteLine("Status       : " + status);

                                var request = (HttpWebRequest)WebRequest.Create(endPoint);

                                Orders ord = new Orders();

                                ord.orderId = orderId;
                                ord.buzzerNo = "";
                                ord.salesNo = "";
                                ord.merchantKey = MERCHANT_KEY;
                                ord.paymentType = "CASHPAID";
                                ord.queueNo = queueNo;
                                ord.status = status;
                                ord.isVoid = isVoid;
                                ord.pin = "1234";

                                var data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(ord));

                                request.Method = "POST";
                                request.ContentType = "application/json";
                                request.ContentLength = data.Length;

                                using (var stream = request.GetRequestStream())
                                {
                                    stream.Write(data, 0, data.Length);
                                }

                                var response = (HttpWebResponse)request.GetResponse();

                                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                                var resp = (JObject)JsonConvert.DeserializeObject(responseString);

                                //remove file after close table in raptor
                                if (status == "3")
                                {
                                    if (!KIOSK_POS.Contains(pos))
                                    {
                                        File.Delete("orders\\" + selectedFile);
                                    }
                                }

                                if (type == "INSERT")
                                {
                                    var id = resp.SelectToken("data[0].id").ToString();
                                    Console.WriteLine("ID in MC : " + id);
                                    File.WriteAllText(string.Concat(new string[] { "orders/", id + "-" + queueNo, ".txt" }), id + "#" + queueNo + "#" + status);
                                }

                                Console.WriteLine(responseString);
                                Console.WriteLine("Connection : Succesfully.");


                            }


                        }
                        catch (Exception r)
                        {
                            
                            Console.WriteLine(r.Message);
                            Console.WriteLine("Connection : Failed.");
                        }

                        Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> End Watch.");

                       Thread.Sleep(500);
                    }
                    
                    connection.Close();
                    
                }
               
            }
            catch (Exception m)
            {
                Console.WriteLine(m.Message);
            }
            

        }
        
        public static void searchFile(string file)
        {
            string partialName = file;
            DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo("orders");
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles("*" + partialName + "*.*");
            
            foreach (FileInfo foundFile in filesInDir)
            {
                string fullName = foundFile.FullName;
                Console.WriteLine(fullName);
            }
        }
        private void OnQueueChange(object s, SqlNotificationEventArgs e)
        {
            //Write code for you task 
            SqlDependency dependency = (SqlDependency)s;
            dependency.OnChange -= OnQueueChange;
            
            Console.WriteLine("triggered");
        }
        public void StopNotification()
        {
            SqlDependency.Stop(CONNECTION, "SBReceiveQueue");
        }
        public bool IsServerConnected()
        {
            using (var l_oConnection = new SqlConnection(CONNECTION))
            {
                try
                {
                    l_oConnection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
            }
        }
    }
}
